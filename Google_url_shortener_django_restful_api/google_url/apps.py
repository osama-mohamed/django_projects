from django.apps import AppConfig


class GoogleUrlConfig(AppConfig):
    name = 'google_url'
